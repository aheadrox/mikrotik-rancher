FROM rancher/vm-base:0.0.3

MAINTAINER Denis Grigoryev dgrigoryev@maprox.net

RUN apt-get -qq update && apt-get install unzip && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
ADD http://download2.mikrotik.com/routeros/6.34.6/chr-6.34.6.img.zip ./chr-6.34.6.img.zip
RUN unzip ./chr-6.34.6.img.zip -d /base_image/ && ls /base_image && rm ./chr-6.34.6.img.zip

EXPOSE 80 8291
